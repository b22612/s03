-- SELECT * FROM <table name>

INSERT INTO artists (name) VALUES ("Psy");
INSERT INTO artists (name) VALUES ("Rivermaya");


INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Psy 6", "2012-07-15", 1);

INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Trip", "1996-02-14", 2);

-- HH:MM:SS -> HHMMSS
INSERT INTO songs (song_name, length, genre, albums_id) VALUES ("Gangnam Style", 339, "KPOP", 1);


INSERT INTO songs (song_name, length, genre, albums_id) VALUES ("Kundiman", 524, "OPM", 2);
INSERT INTO songs (song_name, length, genre, albums_id) VALUES ("Kisapmata", 441, "OPM", 2);

-- [SECTION] Selecting Records
-- Syntax: 
    -- SELECT * FROM table_name; (shows the full table)
    -- SELECT column_nameA,column_nameB FROM table_name;

SELECT * FROM songs;

-- Display the title and genre of all songs
SELECT song_name, genre FROM songs;

-- Display the album title and date released of all albums

SELECT album_title, date_released FROM albums;


-- "WHERE" clause is used to filter records and to extract only those records that fulfill a specific condition
-- Display song titles of songs with genre OPM
SELECT song_name FROM songs WHERE genre = "OPM";

-- AND and OR Keyword
-- Display the title and length of the OPM songs that are more than 4 minutes 30 secs
SELECT song_name, length FROM songs WHERE length > 430 AND genre = "OPM";

-- [SECTION] Updating Records
-- Syntax: UPDATE table_name SET column_nameA = updated_valueA WHERE condition;

-- Update the length of Kundiman to 4 minutes 24 secs

UPDATE songs SET length = 424 WHERE song_name = "Kundiman"; 

UPDATE songs SET length = 424 WHERE song_name = "Kundiman" OR genre = "OPM"; 


-- [SECTION] Deleting Records
-- Syntax: DELETE FROM table_name WHERE condition;
    -- NOTE: Removing the WHERE clause will remove all rows in the table.

-- DELETE all OPM songs that are more than 4 mins 3 secs

DELETE FROM songs WHERE genre = "OPM" AND length > 430; 






